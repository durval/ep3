
  
<h1 align="center">
  <br>
  <a href="https://images.vexels.com/media/users/3/137577/isolated/preview/75b3c8a4aba24222274e4ea2f2aa52ab-cube-logotipo-abstrato-geom-trico-by-vexels.png"><img src="https://images.vexels.com/media/users/3/137577/isolated/preview/75b3c8a4aba24222274e4ea2f2aa52ab-cube-logotipo-abstrato-geom-trico-by-vexels.png" alt="Adiantaê" width="200"></a>
  <br>
  Adiantaê
  <br>
</h1>

<h4 align="center">Um pequeno aplicativo de gerenciamento de projetos construído com Ruby on Rails</a></h4>

<p align="center">
<iframe src="https://giphy.com/embed/37R9MI43U93xf24t86" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/vidsa-37R9MI43U93xf24t86"></a>
</p>

## Sobre o trabalho


Este projeto contém um site feito com o **Ruby on Rails** que se dispõe a facilitar o gerenciamento de equipes. 

As principais funcionalidades são:
  - Cadastro de novos usuários
  - Autentificação por e-mail
  - Envio automático de convites para o e-mails de novos usuários
  - Criação de tarefas para equipes cadastradas
  - Visualização do histório de tarefas de equipes cadastradas

## Instalação

Clone o repositório e siga os passos abaixo no seu terminal:
```bash
# Clone o repositório
$ git clone https://gitlab.com/durvalcsouza/ep3

# Entre no diretório
$ cd ep3/

# Instale as dependências
$ apt install ruby rails
$ yarn install
$ bundle install

# Instale o mailcatcher
$ `gem install mailcatcher`
$ `mailcatcher`

# Acesse o localhost 1080
http://localhost:1080/

# Inicie o banco de dados
$ rake db:migrate

# Execute o servidor
$ rails server

# Entre no localhost 3000 no seu navegador
http://localhost:3000/
```

Nota: Esses passos foram pensados para distribuições Debian (Ubuntu, Mint e outros )

## Screenshots

![dashboard](gitimg/dash.png)
![Editar Projeto](gitimg/edit_project.png)
![Editar Usuário](gitimg/edit_user.png)
![New Project](gitimg/new_project.png)
![New team](gitimg/new_team.png)
![Team](gitimg/team.png)
